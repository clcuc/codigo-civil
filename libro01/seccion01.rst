==================================
SECCION PRIMERA Personas Naturales
==================================

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

TITULO I Principio de la Persona
================================

CAPITULO UNICO
##############

Artículo 1.  Sujeto de Derecho
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La persona humana es sujeto de derecho desde su nacimiento.

La vida humana comienza con la concepción. El concebido es sujeto de derecho para todo cuanto le favorece. La atribución de derechos patrimoniales está condicionada a que nazca vivo.

CONCORDANCIAS:     R.M. Nº 389-2004-MINSA (Precisan que la expedición del Certificado de Nacido Vivo es gratuita en todos los establecimientos de
salud del país, públicos y privados)
R.M.Nº 148-2012-MINSA (Aprueban Directiva Administrativa que establece procedimiento para el registro del Certificado de Nacido Vivo en
todos los establecimientos de salud del país)

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA


Artículo 2.  Reconocimiento del embarazo o parto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La mujer puede solicitar judicialmente el reconocimiento de su embarazo o del parto, con citación de las personas que tengan interés en el nacimiento.

"La solicitud se tramita como prueba anticipada, con citación de las personas que por indicación de la solicitante o a criterio del Juez, puedan tener derechos que resulten afectados. El Juez puede ordenar de oficio la actuación de los medios probatorios que estime pertinentes. En este proceso no se admite oposición." (*)

(*) Párrafo agregado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993.

TITULO II Derechos de la Persona
================================

CAPITULO UNICO
##############

Artículo 3.  Capacidad de Goce
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Toda persona tiene el goce de los derechos civiles, salvo las excepciones expresamente establecidas por ley.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

“Artículo 3.- Capacidad jurídica

Toda persona tiene capacidad jurídica para el goce y ejercicio de sus derechos.

La capacidad de ejercicio solo puede ser restringida por ley. Las personas con discapacidad tienen capacidad de ejercicio en igualdad de condiciones en todos los aspectos de la vida.”


Artículo 4.  Igualdad entre varón y mujer en el goce y ejercicio de sus derechos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El varón y la mujer tienen igual capacidad de goce y de ejercicio de los derechos civiles.


Artículo 5.  Irrenunciabilidad de los derechos fundamentales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El derecho a la vida, a la integridad física, a la libertad, al honor y demás inherentes a la persona humana son irrenunciables y no pueden ser objeto de cesión. Su ejercicio no puede sufrir limitación voluntaria, salvo lo dispuesto en el artículo 6.


Artículo 6.  Actos de disposición del propio cuerpo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los actos de disposición del propio cuerpo están prohibidos cuando ocasionen una disminución permanente de la integridad física o cuando de alguna manera sean contrarios al orden público o a las buenas costumbres. Empero, son válidos si su exigencia corresponde a un estado de necesidad, de orden médico o quirúrgico o si están inspirados por motivos humanitarios.

Los actos de disposición o de utilización de órganos y tejidos de seres humanos son regulados por la ley de la materia.


Artículo 7.  Donación de órganos o tejidos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La donación de partes del cuerpo o de órganos o tejidos que no se regeneran no debe perjudicar gravemente la salud o reducir sensiblemente el tiempo de vida del donante. Tal disposición está sujeta a consentimiento expreso y escrito del donante.


Artículo 8.  Disposición del cuerpo pos morten
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Es válido el acto por el cual una persona dispone altruistamente de todo o parte de su cuerpo para que sea utilizado, después de su muerte, con fines de interés social o para la prolongación de la vida humana.

La disposición favorece sólo a la persona designada como beneficiaria o a instituciones científicas, docentes, hospitalarias o banco de órganos o tejidos, que no persigan fines de lucro.


Artículo 9.  Revocación de la donación del cuerpo humano
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es revocable, antes de su consumación, el acto por el cual una persona dispone en vida de parte de su cuerpo, de conformidad con el artículo 6. Es también revocable el acto por el cual la persona dispone, para después de su muerte, de todo o parte de su cuerpo.

La revocación no da lugar al ejercicio de acción alguna.


Artículo 10.  Disposición del cadáver por entidad competente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El jefe del establecimiento de salud o el del servicio de necropsias donde se encuentre un cadáver puede disponer de parte de éste para la conservación o prolongación de la vida humana, previo conocimiento de los parientes a que se refiere el artículo 13. No procede la disposición si existe oposición de éstos, manifestada dentro del plazo, circunstancias y responsabilidades que fija la ley de la materia.

Los mismos funcionarios pueden disponer del cadáver no identificado o abandonado, para los fines del artículo 8, de conformidad con la ley de la materia.(*)

(*) Artículo modificado por la Única Disposición Complementaria Modificatoria de la Ley N° 30473, publicada el 29 junio 2016, cuyo texto es el siguiente:


Artículo 10.  “Disposición del cadáver por entidad competente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El jefe del establecimiento de salud o el del servicio de necropsias donde se encuentre un cadáver puede disponer de parte de éste para la conservación o prolongación de la vida humana, con conocimiento de los parientes a que se refiere el artículo 13.

Los mismos funcionarios pueden disponer del cadáver no identificado o abandonado, para los fines del artículo 8, de conformidad con la ley de la materia”.


Artículo 11.  Validez de obligación de sometimiento a examen médico
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son válidas las estipulaciones por las que una persona se obliga a someterse a examen médico, siempre que la conservación de su salud o aptitud síquica o física sea motivo determinante de la relación contractual.


Artículo 12.  Inexigibilidad de contratos peligrosos para la persona
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No son exigibles los contratos que tengan por objeto la realización de actos excepcionalmente peligrosos para la vida o la integridad física de una persona, salvo que correspondan a su actividad habitual y se adopten las medidas de previsión y seguridad adecuadas a las circunstancias.


Artículo 13.  Actos funerarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A falta de declaración hecha en vida, corresponde al cónyuge del difunto, a sus descendientes, ascendientes o hermanos, excluyentemente y en este orden, decidir sobre la necropsia, la incineración y la sepultura sin perjuicio de las normas de orden público pertinentes.


Artículo 14.  Derecho a la intimidad personal y familiar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La intimidad de la vida personal y familiar no puede ser puesta de manifiesto sin el asentimiento de la persona o si ésta ha muerto, sin el de su cónyuge, descendientes, ascendientes o hermanos, excluyentemente y en este orden.


Artículo 15.  Derecho a la imagen y voz
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La imagen y la voz de una persona no pueden ser aprovechadas sin autorización expresa de ella o, si ha muerto, sin el asentimiento de su cónyuge, descendientes, ascendientes o hermanos, excluyentemente y en este orden.

Dicho asentimiento no es necesario cuando la utilización de la imagen y la voz se justifique por la notoriedad de la persona, por el cargo que desempeñe, por hechos de importancia o interés público o por motivos de índole científica, didáctica o cultural y siempre que se relacione con hechos o ceremonias de interés general que se celebren en público. No rigen estas excepciones cuando la utilización de la imagen o la voz atente contra el honor, el decoro o la reputación de la persona a quien corresponden.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA


Artículo 16.  Confidencialidad de la correspondencia y demás comunicaciones
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La correspondencia epistolar, las comunicaciones de cualquier género o las grabaciones de la voz, cuando tengan carácter confidencial o se refieran a la intimidad de la vida personal y familiar, no pueden ser interceptadas o divulgadas sin el asentimiento del autor y, en su caso, del destinatario. La publicación de las memorias personales o familiares, en iguales circunstancias, requiere la autorización del autor.

Muertos el autor o el destinatario, según los casos, corresponde a los herederos el derecho de otorgar el respectivo asentimiento. Si no hubiese acuerdo entre los herederos, decidirá el juez.

La prohibición de la publicación póstuma hecha por el autor o el destinatario no puede extenderse más allá de cincuenta años a partir de su muerte.


Artículo 17.  Defensa de los derechos de la persona
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La violación de cualquiera de los derechos de la persona a que se refiere este título, confiere al agraviado o a sus herederos acción para exigir la cesación de los actos lesivos.

La responsabilidad es solidaria.


Artículo 18.  Protección de los derechos de autor e inventor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los derechos del autor o del inventor, cualquiera sea la forma o modo de expresión de su obra, gozan de protección jurídica de conformidad con la ley de la materia.

TITULO III Nombre
=================

CAPITULO UNICO
##############

Artículo 19.  Derecho al nombre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Toda persona tiene el derecho y el deber de llevar un nombre. Este incluye los apellidos.

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA
JURISPRUDENCIA DE LA CORTE INTERAMERICANA DE DERECHOS HUMANOS


Artículo 20.  Nombre del hijo matrimonial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Al hijo matrimonial le corresponden el primer apellido del padre y el primero de la madre. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28720, publicada el 25 abril 2006, cuyo texto es el siguiente:

"Artículo 20.- Apellidos del hijo

Al hijo le corresponde el primer apellido del padre y el primero de la madre."


Artículo 21.  Nombre del hijo extramatrimonial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Al hijo extramatrimonial le corresponden los apellidos del progenitor que lo haya reconocido. Si es reconocido por ambos lleva el primer apellido de los dos.

Rige la misma regla en caso de filiación por declaración judicial. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28720, publicada el 25 abril 2006, cuyo texto es el siguiente:

"Artículo 21.- Inscripción del nacimiento

Cuando el padre o la madre efectúe separadamente la inscripción del nacimiento del hijo nacido fuera del vínculo matrimonial, podrá revelar el nombre de la persona con quien lo hubiera tenido. En este supuesto, el hijo llevará el apellido del padre o de la madre que lo inscribió, así como del presunto progenitor, en este último caso no establece vínculo de filiación.

Luego de la inscripción, dentro de los treinta (30) días, el registrador, bajo responsabilidad, pondrá en conocimiento del presunto progenitor tal hecho, de conformidad con el reglamento.

Cuando la madre no revele la identidad del padre, podrá inscribir a su hijo con sus apellidos.”  (*)

(*) De conformidad con el Artículo 2 de la Ley N° 28720, publicada el 25 abril 2006, el progenitor que de mala fe imputara la paternidad o maternidad de su hijo a persona distinta con la que hubiera tenido el hijo, será pasible de las responsabilidades y sanciones civiles y penales que correspondan. 

(*) De conformidad con el Artículo 3 de la Ley N° 28720, publicada el 25 abril 2006, el presunto progenitor que se considere afectado por la consignación de su nombre en la partida de nacimiento de un niño que no ha reconocido, puede iniciar un proceso de usurpación de nombre, de conformidad a lo establecido en el artículo 28 del Código Civil, y de acuerdo a la vía del proceso sumarísimo.


Artículo 22.  Nombre del adoptado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El adoptado lleva los apellidos del adoptante o adoptantes.

"El hijo de uno de los cónyuges o concubinos puede ser adoptado por el otro. En tal caso, lleva como primer apellido el del padre adoptante y como segundo el de la madre biológica o, el primer apellido del padre biológico y el primer apellido de la madre adoptante, según sea el caso.” (*)

(*) Párrafo final incorporado por el Artículo Único de la Ley N° 30084, publicada el 22 septiembre 2013.


Artículo 23.  Nombre del recién nacido de padres desconocidos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El recién nacido cuyos progenitores son desconocidos debe ser inscrito con el nombre adecuado que le asigne el registrador del estado civil.


Artículo 24.  Derecho de la mujer a llevar el apellido del marido
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La mujer tiene derecho a llevar el apellido del marido agregado al suyo y a conservarlo mientras no contraiga nuevo matrimonio. Cesa tal derecho en caso de divorcio o nulidad de matrimonio.

Tratándose de separación de cuerpos, la mujer conserva su derecho a llevar el apellido del marido. En caso de controversia resuelve el juez.


Artículo 25.  Prueba del nombre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La prueba referente al nombre resulta de su respectiva inscripción en los registros de estado civil.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA


Artículo 26.  Defensa del derecho al nombre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Toda persona tiene derecho a exigir que se le designe por su nombre.

Cuando se vulnere este derecho puede pedirse la cesación del hecho violatorio y la indemnización que corresponda.


Artículo 27.  Nulidad de convenios sobre el nombre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es nulo el convenio relativo al nombre de una persona natural, salvo para fines publicitarios, de interés social y los que establece la ley.


Artículo 28.  Indemnización por usurpación de nombre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Nadie puede usar nombre que no le corresponde. El que es perjudicado por la usurpación de su nombre tiene acción para hacerla cesar y obtener la indemnización que corresponda.

CONCORDANCIAS:     Ley N° 28720, Art. 3 (Acción de usurpación de nombre)


Artículo 29.  Cambio o adición de nombre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Nadie puede cambiar su nombre ni hacerle adiciones, salvo por motivos justificados y mediante autorización judicial, debidamente publicada e inscrita.

El cambio o adición del nombre alcanza, si fuere el caso, al cónyuge y a los hijos menores de edad.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA


Artículo 30.  Efectos del cambio o adición de nombre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El cambio o adición del nombre no altera la condición civil de quien lo obtiene ni constituye prueba de filiación.


Artículo 31.  Impugnación judicial por cambio o adición de nombre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La persona perjudicada por un cambio o adición de nombre puede impugnarlo judicialmente.


Artículo 32.  Protección jurídica del seudónimo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El seudónimo, cuando adquiere la importancia del nombre, goza de la misma protección jurídica dispensada a éste.

TITULO IV Domicilio
===================

CAPITULO UNICO
##############

Artículo 33.  Domicilio
~~~~~~~~~~~~~~~~~~~~~~~

El domicilio se constituye por la residencia habitual de la persona en un lugar.


Artículo 34.  PROCESOS CONSTITUCIONALES
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se puede designar domicilio especial para la ejecución de actos jurídicos. Esta designación sólo implica sometimiento a la jurisdicción correspondiente, salvo pacto distinto. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

Domicilio especial

"Artículo 34.- Se puede designar domicilio especial para la ejecución de actos jurídicos. Esta designación sólo implica sometimiento a la competencia territorial correspondiente, salvo pacto distinto."


Artículo 35.  Persona con varios domicilios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A la persona que vive alternativamente o tiene ocupaciones habituales en varios lugares se le considera domiciliada en cualquiera de ellos.


Artículo 36.  Domicilio conyugal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El domicilio conyugal es aquel en el cual los cónyuges viven de consuno o, en su defecto, el último que compartieron.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA


Artículo 37.  Domicilio del incapaz
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los incapaces tienen por domicilio el de sus representantes legales.


Artículo 38.  Domicilio de funcionarios públicos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los funcionarios públicos están domiciliados en el lugar donde ejercen sus funciones, sin perjuicio, en su caso, de lo dispuesto en el artículo 33.

El domicilio de las personas que residen temporalmente en el extranjero, en ejercicio de funciones del Estado o por otras causas, es el último que hayan tenido en el territorio nacional.


Artículo 39.  Cambio de domicilio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El cambio de domicilio se realiza por el traslado de la residencia habitual a otro lugar.


Artículo 40.  Comunicación a terceros del cambio de domicilio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El cambio de domicilio no puede oponerse a los acreedores si no ha sido puesto en su conocimiento mediante comunicación indubitable. (*)

(*) Artículo sustituido por el Artículo 1 de la Ley N° 27723, publicada el 14 mayo 2002, cuyo texto es el siguiente:

"Artículo 40.-Oposición al cambio de domicilio

El deudor deberá comunicar al acreedor el cambio de domicilio señalado para el cumplimiento de la prestación obligacional, dentro de los treinta (30) días de ocurrido el hecho, bajo responsabilidad civil y/o penal a que hubiere lugar.

El deudor y los terceros ajenos a la relación obligacional con el acreedor, están facultados para oponer a éste el cambio de su domicilio.

La oponibilidad al cambio de domicilio se efectuará mediante comunicación indubitable.”


Artículo 41.  Personas sin residencia habitual
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A la persona que no tiene residencia habitual se le considera domiciliada en el lugar donde se encuentre.

TITULO V Capacidad e Incapacidad de Ejercicio
=============================================

CAPITULO UNICO
##############

Artículo 42.  Plena capacidad de ejercicio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tienen plena capacidad de ejercicio de sus derechos civiles las personas que hayan cumplido dieciocho años de edad, salvo lo dispuesto en los artículos 43 y 44. (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

“Artículo 42.- Capacidad de ejercicio plena

Toda persona mayor de dieciocho años tiene plena capacidad de ejercicio. Esto incluye a todas las personas con discapacidad, en igualdad de condiciones con las demás y en todos los aspectos de la vida, independientemente de si usan o requieren de ajustes razonables o apoyos para la manifestación de su voluntad.

Excepcionalmente tienen plena capacidad de ejercicio los mayores de catorce años y menores de dieciocho años que contraigan matrimonio, o quienes ejerciten la paternidad.”

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA


Artículo 43.  Incapacidad absoluta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son absolutamente incapaces:

1.- Los menores de dieciséis años, salvo para aquellos actos determinados por la ley.(*) RECTIFICADO POR FE DE ERRATAS

2.- Los que por cualquier causa se encuentren privados de discernimiento.(*)

(*) Numeral derogado por el Literal a) de la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

CONCORDANCIAS:     D.LEG.N° 1310, Art. 4 (Curatela especial para personas adultas mayores para efectos pensionarios y devolución del FONAVI)

3.- Los sordomudos, los ciegosordos y los ciegomudos que no pueden expresar su voluntad de manera indubitable. (*)

(*) Numeral derogado por la Única Disposición Complementaria Derogatoria de la Ley N° 29973, publicada el 24 diciembre 2012.

CONCORDANCIAS:     D.S. N° 014-2005-SA, Art. 35   (Condiciones y requisitos del donante cadavérico)


Artículo 44.  Incapacidad relativa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son relativamente incapaces: (*)

(*) Extremo modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

"Artículo 44.- Capacidad de ejercicio restringida

Tienen capacidad de ejercicio restringida:"

1.- Los mayores de dieciséis y menores de dieciocho años de edad.
2.- Los retardados mentales. (*)

(*) Numeral derogado por el Literal a) de la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

3.- Los que adolecen de deterioro mental que les impide expresar su libre voluntad.(*)

(*) Numeral derogado por el Literal a) de la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

4.- Los pródigos.
5.- Los que incurren en mala gestión.
6.- Los ebrios habituales.
7.- Los toxicómanos.
8.- Los que sufren pena que lleva anexa la interdicción civil.
"9.- Las personas que se encuentren en estado de coma, siempre que no hubiera designado un apoyo con anterioridad.”(*)(**)

(*) Numeral incorporado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

(**) De conformidad con la Tercera Disposición Complementaria Final del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, las personas señaladas en los numerales 6 y 7 del presente artículo que cuentan con certificado de discapacidad pueden designar apoyos y salvaguardias para el ejercicio de su capacidad jurídica.

CONCORDANCIAS:     D.S. N° 014-2005-SA, Art. 35   (Condiciones y requisitos del donante cadavérico)
D.LEG.N° 1310, Art. 4 (Curatela especial para personas adultas mayores para efectos pensionarios y devolución del FONAVI)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA


Artículo 45.  Representante legal de incapaces
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 Los representantes legales de los incapaces ejercen los derechos civiles de éstos, según las normas referentes a la patria potestad, tutela y curatela. (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

“Artículo 45.- Ajustes razonables y apoyo

Toda persona con discapacidad que requiera ajustes razonables o apoyo para el ejercicio de su capacidad jurídica puede solicitarlos o designarlos de acuerdo a su libre elección.”

“Artículo 45- A.- Representantes Legales

Las personas con capacidad de ejercicio restringida contempladas en los numerales 1 al 8 del artículo 44 contarán con un representante legal que ejercerá los derechos según las normas referidas a la patria potestad, tutela o curatela.”(*)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

“Artículo 45-B- Designación de apoyos y salvaguardias

Pueden designar apoyos y salvaguardias:

1. Las personas con discapacidad que manifiestan su voluntad puede contar con apoyos y salvaguardias designados judicial o notarialmente.

2. Las personas con discapacidad que no pueden manifestar su voluntad podrán contar con apoyos y salvaguardias designados judicialmente.

3. Las personas que se encuentren en estado de coma que hubieran designado un apoyo con anterioridad mantendrán el apoyo designado.

4. Las personas con capacidad de ejercicio restringida contempladas en el numeral 9 del artículo 44 contarán con los apoyos y salvaguardias establecidos judicialmente, de conformidad con las disposiciones del artículo 659-E del presente Código.”(*)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

Artículo 46.- La incapacidad de las personas mayores de dieciséis años cesa por matrimonio o por obtener título oficial que les autorice para ejercer una profesión u oficio. (*) RECTIFICADO POR FE DE ERRATAS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tratándose de mujeres mayores de catorce años cesa también por matrimonio.

La capacidad adquirida por matrimonio no se pierde por la terminación de éste. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 27201, publicada el 14 noviembre 1999, cuyo texto es el siguiente:

Fin de la incapacidad de mayores de 16 años por matrimonio o título

"Artículo 46.- La incapacidad de las personas mayores de dieciséis años cesa por matrimonio o por obtener título oficial que les autorice para ejercer una profesión u oficio.

La capacidad adquirida por matrimonio no se pierde por la terminación de éste.

Tratándose de mayores de catorce años cesa la incapacidad a partir del nacimiento del hijo, para realizar solamente los siguientes actos:

1. Reconocer a sus hijos.
2. Reclamar o demandar por gastos de embarazo y parto.
3. Demandar y ser parte en los procesos de tenencia y alimentos a favor de sus hijos." (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 29274, publicada el 28 octubre 2008, cuyo texto es el siguiente:

"Artículo 46.-Capacidad adquirida por matrimonio o título oficial

La incapacidad de las personas mayores de dieciséis (16) años cesa por matrimonio o por obtener título oficial que les autorice para ejercer una profesión u oficio.

La capacidad adquirida por matrimonio no se pierde por la terminación de éste.

Tratándose de mayores de catorce (14) años cesa la incapacidad a partir del nacimiento del hijo, para realizar solamente los siguientes actos:

1. Reconocer a sus hijos.

2. Demandar por gastos de embarazo y parto.

3. Demandar y ser parte en los procesos de tenencia y alimentos a favor de sus hijos.

4. Demandar y ser parte en los procesos de filiación extramatrimonial de sus hijos.” (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1377, publicado el 24 agosto 2018, cuyo texto es el siguiente:

«Artículo 46.- Capacidad adquirida por matrimonio o título oficial

La incapacidad de las personas mayores de dieciséis (16) años cesa por matrimonio o por obtener título oficial que les autorice para ejercer una profesión u oficio.

La capacidad adquirida por matrimonio no se pierde por la terminación de este.

Tratándose de mayores de catorce (14) años cesa la incapacidad a partir del nacimiento del hijo o la hija, para realizar solamente los siguientes actos:

1. Inscribir el nacimiento y reconocer a sus hijos e hijas.

2. Demandar por gastos de embarazo y parto.

3. Demandar y ser parte en los procesos de tenencia, alimentos y régimen de visitas a favor de sus hijos e hijas.

4. Demandar y ser parte en los procesos de filiación extramatrimonial de sus hijos e hijas.

5. Celebrar conciliaciones extrajudiciales a favor de sus hijos e hijas.

6. Solicitar su inscripción en el Registro Único de Identificación de Personas Naturales, tramitar la expedición y obtener su Documento Nacional de Identidad.

7. Impugnar judicialmente la paternidad.»

TITULO VI Ausencia
==================

CAPITULO PRIMERO
################


Artículo 47.  Desaparición
~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando una persona no se halla en el lugar de su domicilio y se carece de noticias sobre su paradero, el juez de primera instancia del último domicilio o del lugar donde se encuentren sus bienes puede proceder, a petición de parte interesada o del Ministerio Público, a la designación de curador interino. (*)

(*) Primer párrafo modificado por el Artículo 5 del Decreto Ley N° 25940, publicado el 11 diciembre 1992, cuyo texto es el siguiente:

"Cuando una persona no se halla en el lugar de su domicilio y han transcurrido más de sesenta días sin noticias sobre su paradero, cualquier familiar hasta el cuarto grado de consanguinidad o afinidad, excluyendo el más próximo al más remoto, pueden solicitar la designación de curador interino. También puede solicitarlo quien invoque legítimo interés en los negocios o asuntos del desaparecido, con citación de los familiares conocidos y del Ministerio Público. La solicitud se tramita como proceso no contencioso."

No procede tal designación si el desaparecido tiene mandatario con facultades suficientes. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993. La misma que recoge las modificaciones hechas anteriormente a este artículo por la Primera Disposición Modificatoria del Decreto Legislativo Nº 768, publicado el 04 marzo 1992 y la del Artículo 5 del Decreto Ley N° 25940, publicado el 11-12-92, cuyo texto es el siguiente:

Nombramiento de curador por desaparición

"Artículo 47.- Cuando una persona no se halla en el lugar de su domicilio y han transcurrido más de sesenta días sin noticias sobre su paradero, cualquier familiar hasta el cuarto grado de consanguinidad o afinidad, excluyendo el más próximo al más remoto, pueden solicitar la designación de curador interino. También puede solicitarlo quien invoque legítimo interés en los negocios o asuntos del desaparecido, con citación de los familiares conocidos y del Ministerio Público. La solicitud se tramita como proceso no contencioso.

No procede la designación de curador si el desaparecido tiene representante o mandatario con facultades suficientes inscritas en el registro público."

Artículo 48.- Normas que rigen la curatela del desaparecido
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La curatela a que se contrae el artículo 47 se rige por las disposiciones de los artículos 564 a 618, en cuanto sean pertinentes.

CAPITULO SEGUNDO
################

Declaración de Ausencia


Artículo 49.  Declaración judicial de ausencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Transcurridos dos años desde que se tuvo la última noticia del desaparecido, cualquiera que tenga legítimo interés o el Ministerio Público pueden solicitar la declaración judicial de ausencia.

Es competente el juez del último domicilio que tuvo el desaparecido o el del lugar donde se encuentre la mayor parte de sus bienes.


Artículo 50.  Posesión temporal de los bienes del ausente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En la declaración judicial de ausencia se ordenará dar la posesión temporal de los bienes del ausente a quienes serían sus herederos forzosos al tiempo de dictarla.

Si no hubiere persona con esta calidad continuará, respecto a los bienes del ausente, la curatela establecida en el artículo 47.


Artículo 51.  Facultades y límites del poseedor de bienes del ausente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La posesión temporal de los bienes del ausente, a que se refiere el artículo 50, debe ser precedida de la formación del respectivo inventario valorizado.

El poseedor tiene los derechos y obligaciones inherentes a la posesión y goza de los frutos con la limitación de reservar de éstos una parte igual a la cuota de libre disposición del ausente.


Artículo 52.  Indisponibilidad de los bienes del ausente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quienes hubieren obtenido la posesión temporal de los bienes del ausente no pueden enajenarlos ni gravarlos, salvo casos de necesidad o utilidad con sujeción al artículo 56.


Artículo 53.  Inscripción de la declaración judicial de ausencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La declaración judicial de ausencia debe ser inscrita en el registro de mandatos y poderes para extinguir los otorgados por el ausente.


Artículo 54.  Designación del administrador judicial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A solicitud de cualquiera que haya obtenido la posesión temporal de los bienes del ausente, se procede a la designación de administrador judicial.


Artículo 55.  Derechos y obligaciones del administrador judicial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son derechos y obligaciones del administrador judicial de los bienes del ausente:

1.- Percibir los frutos.

2.- Pagar las deudas del ausente y atender los gastos correspondientes al patrimonio que administra.

3.- Reservar en cuenta bancaria, o con las seguridades que señale el juez, la cuota a que se refiere el artículo 51.

4.- Distribuir regularmente entre las personas que señala el artículo 50 los saldos disponibles, en proporción a sus eventuales derechos sucesorios.

5.- Ejercer la representación judicial del ausente con las facultades especiales y generales que la ley confiere, excepto las que importen actos de disposición.

6.- Ejercer cualquier otra atribución no prevista, si fuere conveniente al patrimonio bajo su administración, previa autorización judicial.

7.- Rendir cuenta de su administración en los casos señalados por la ley.


Artículo 56.  Autorización judicial para disponer de los bienes del ausente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En caso de necesidad o utilidad y previa autorización judicial, el administrador puede enajenar o gravar bienes del ausente en la medida de lo indispensable.


Artículo 57.  Aplicación supletoria de normas de ordenamiento procesal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En lo no previsto por los artículos 55 y 56 se aplican las disposiciones del Código de Procedimientos Civiles sobre administración judicial de bienes comunes.


Artículo 58.  Alimentos para herederos forzosos del ausente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El cónyuge del ausente u otros herederos forzosos económicamente dependientes de él, que no recibieren rentas suficientes para atender a sus necesidades alimentarias, pueden solicitar al juez la asignación de una pensión, cuyo monto será señalado según la condición económica de los solicitantes y la cuantía del patrimonio afectado.

"Esta pretensión se tramita conforme al proceso sumarísimo de alimentos, en lo que resulte aplicable." (*)

(*) Párrafo agregado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993.


Artículo 59.  Fin de la declaración judicial de ausencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cesan los efectos de la declaración judicial de ausencia por:

1.- Regreso del ausente.

2.- Designación de apoderado con facultades suficientes, hecha por el ausente con posterioridad a la declaración.

3.- Comprobación de la muerte del ausente.

4.- Declaración judicial de muerte presunta.

Artículo 60.- En los casos de los incisos 1 y 2 del artículo 59 se restituye a su titular el patrimonio, en el estado en que se encuentre. La resolución es dictada dentro del procedimiento de declaración judicial de ausencia.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los casos de los incisos 3 y 4 del artículo 59 se procede a la apertura de la sucesión. (*)

(*) Artículo modificado por el Artículo 5 del Decreto Ley N° 25940, publicado el 11 diciembre 1992, cuyo texto es el siguiente:

"Artículo 60.- En los casos de los incisos 1 y 2 del Artículo 59 se restituye a su titular el patrimonio, en el estado en que se encuentre. La petición se tramita como proceso no contencioso con citación de quienes solicitaron la declaración de ausencia.

En los casos de los incisos 3 y 4 del artículo 59, se procede a la apertura de la sucesión." (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993. La misma que recoge las modificaciones hechas anteriormente a este artículo por la Primera Disposición Modificatoria del Decreto Legislativo Nº 768, publicado el 04 marzo 1992 y la del Artículo 5 del Decreto Ley N° 25940, publicado el 11 diciembre 1992, cuyo texto es el siguiente:

Restitución o sucesión del patrimonio del ausente

"Artículo 60.- En los casos de los incisos 1 y 2 del artículo 59 se restituye a su titular el patrimonio, en el estado en que se encuentre. La petición se tramita como proceso no contencioso con citación de quienes solicitaron la declaración de ausencia.

En los casos de los incisos 3 y 4 del artículo 59, se procede a la apertura de la sucesión".

TITULO VII Fin de la Persona
============================

CAPITULO PRIMERO
################

Muerte

CONCORDANCIAS:     R.J. N° 782-2009-JNAC-RENIEC (Precisan que el Acta de Defunción constituye un instrumento jurídico que acredita el fallecimiento)
R.J. N° 771-2010-JNAC-RENIEC (Precisan que la inscripción de la Defunción en las Oficinas de Registros del Estado Civil, Oficinas Registrales y
Oficinas Registrales Auxiliares del RENIEC a nivel nacional, no se encuentra sujeta a plazo alguno)


Artículo 61.  Fin de la persona
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La muerte pone fin a la persona.


Artículo 62.  Conmorencia
~~~~~~~~~~~~~~~~~~~~~~~~~

Si no se puede probar cuál de dos o más personas murió primero, se las reputa muertas al mismo tiempo y entre ellas no hay trasmisión de derechos hereditarios.

CAPITULO SEGUNDO
################

Declaración de Muerte Presunta


Artículo 63.  Procedencia de declaración judicial de muerte presunta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Procede la declaración de muerte presunta, sin que sea indispensable la de ausencia, a solicitud de cualquier interesado o del Ministerio Público en los siguientes casos:

1.- Cuando hayan transcurrido diez años desde las últimas noticias del desaparecido o cinco si éste tuviere más de ochenta años de edad.

2.- Cuando hayan transcurrido dos años si la desaparición se produjo en circunstancias constitutivas de peligro de muerte. El plazo corre a partir de la cesación del evento peligroso.

3.- Cuando exista certeza de la muerte, sin que el cadáver sea encontrado o reconocido.


Artículo 64.  Efectos de la declaración de muerte presunta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La declaración de muerte presunta disuelve el matrimonio del desaparecido. Dicha resolución se inscribe en el registro de defunciones.


Artículo 65.  Contenido de la resolución de muerte presunta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En la resolución que declara la muerte presunta se indica la fecha probable y, de ser posible, el lugar de la muerte del desaparecido.


Artículo 66.  Improcedencia de la declaración de muerte presunta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El juez que considere improcedente la declaración de muerte presunta puede declarar la ausencia.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

CAPITULO TERCERO
################


Artículo 67.  Reconocimiento de Existencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La existencia de la persona cuya muerte hubiere sido judicialmente declarada debe ser reconocida a solicitud de ella, de cualquier interesado o del Ministerio Público, dentro del mismo proceso, con citación de quienes intervinieron en éste y sin más trámite que la prueba de la supervivencia. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

Reconocimiento de existencia

"Artículo 67.- La existencia de la persona cuya muerte hubiera sido judicialmente declarada, puede ser reconocida a solicitud de ella, de cualquier interesado, o del Ministerio Público. La pretensión se tramita como proceso no contencioso, con citación de quienes solicitaron la declaración de muerte presunta".


Artículo 68.  Efectos sobre el nuevo matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El reconocimiento de existencia no invalida el nuevo matrimonio que hubiere contraído el cónyuge.


Artículo 69.  Facultad de reivindicar los bienes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El reconocimiento de existencia faculta a la persona para reivindicar sus bienes, conforme a ley.

TITULO VIII Registros del Estado Civil
======================================

CAPITULO UNICO
##############

Artículo 70.  Actos inscribibles en los registros del estado civil
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los registros del estado civil son públicos. En ellos se inscriben los nacimientos, los matrimonios y las defunciones.

El reglamento de dichos registros determina los demás actos inscribibles conforme a ley. (*)

(*) Artículo derogado por la Séptima Disposición Final de la Ley Nº 26497, publicada el 12 julio 1995.


Artículo 71.  Lugares de funcionamiento de los registros del estado civil
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Habrá registros a cargo de las autoridades o funcionarios competentes:

1. En todos los concejos municipales.
2. En los consulados del Perú.
3. En otros lugares donde fueren necesarios, por aplicación de la ley de la materia. (*)

(*) Artículo derogado por la Séptima Disposición Final de la Ley Nº 26497, publicada el 12 julio 1995.


Artículo 72.  Competencia territorial para las inscripciones
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las inscripciones se extienden en el registro del lugar donde ocurran los respectivos hechos, con las formalidades que determina la ley. (*)

(*) Artículo derogado por la Séptima Disposición Final de la Ley Nº 26497, publicada el 12 julio 1995.


Artículo 73.  Valor probatorio de las partidas de registro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las partidas de inscripción prueban los hechos a que se refieren, salvo que se declare judicialmente su nulidad. (*)

(*) Artículo derogado por la Séptima Disposición Final de la Ley Nº 26497, publicada el 12 julio 1995.


Artículo 74.  Rectificaciones o adiciones en las partidas de registro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pueden hacerse rectificaciones o adiciones en las partidas de registro sólo en virtud de resolución judicial, salvo disposición distinta de la ley. (*)

(*) Artículo derogado por la Séptima Disposición Final de la Ley Nº 26497, publicada el 12 julio 1995.


Artículo 75.  Prueba de inscripción por destrucción o pérdida de la partida de registro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La persona afectada por la destrucción o pérdida de las partidas de inscripción puede probar los actos inscribibles por los medios que permite la ley, siempre que se acredite su inexistencia en el registro respectivo. (*)

(*) Artículo derogado por la Séptima Disposición Final de la Ley Nº 26497, publicada el 12 julio 1995.
