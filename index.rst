.. Código Civil documentation master file, created by
   sphinx-quickstart on Sun Jun 21 18:25:37 2020.

Código Civil del Perú
=====================

.. toctree::
   :caption: Título Preliminar
   :name: tit-pre
   :maxdepth: 2

   Sección Única                <libro00/seccion01>

.. toctree::
   :caption: Libro I Derecho de las Personas
   :name: libro-i
   :maxdepth: 2

   S1 Personas Naturales                <libro01/seccion01>
   S2 Personas Jurídicas                <libro01/seccion02>
   S3 Asociación, Fundación y Comité NI <libro01/seccion03>
   S4 Comunidades Campesinas y Nativas  <libro01/seccion04>

.. toctree::
   :caption: Libro II Acto Jurídico
   :name: libro-ii
   :maxdepth: 2

   S-Única Acto Juridico        <libro02/seccion01>

.. toctree::
   :caption: Libro III Derecho de Familia
   :name: libro-iii
   :maxdepth: 2

   S1 Disposiciones Generales   <libro03/seccion01>
   S2 Sociedad Conyugal         <libro03/seccion02>

.. toctree::
   :caption: Libro IV Derecho de Sucesiones
   :name: libro-iv
   :maxdepth: 2

   S2 Sucesion Testamentaria    <libro04/seccion02>
   S3 Sucesion Intestada        <libro04/seccion03>

.. toctree::
   :caption: Libro V Derechos Reales
   :name: libro-v
   :maxdepth: 2

   S3 Derechos Reales Principales   <libro05/seccion03>

.. toctree::
   :caption: Libro VI Las obligaciones
   :name: libro-vi
   :maxdepth: 2

   S2 Efectos de las Obligaciones <libro06/seccion02>

.. toctree::
   :caption: Libro VII Fuentes de las obligaciones
   :name: libro-vii
   :maxdepth: 2

   S2 Contratos nominados               <libro07/seccion02>
   S6 Responsabilidad extracontractual  <libro07/seccion06>

..  libroi/secseg

Indices y tablas
================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

------

Esta versión fue compilada |today|.
