======================================
SECCION SEGUNDA Sucesión Testamentaria
======================================

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

TITULO  I Disposiciones Generales
=================================

CAPITULO UNICO
##############

Artículo 686 Sucesión por testamento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Por el testamento una persona puede disponer de sus bienes, total o parcialmente, para después de su muerte, y ordenar su propia sucesión dentro de los límites de la ley y con las formalidades que ésta señala.

Son válidas las disposiciones de carácter no patrimonial contenidas en el testamento, aunque el acto se limite a ellas.

Artículo 687 Incapacidad para otorgar testamento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son incapaces de otorgar testamento: (*)

(*) Extremo modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

“Artículo 687.- Imposibilitados para otorgar testamento

No pueden otorgar testamento:"

1.- Los menores de edad, salvo el caso previsto en el artículo 46.

2.- Los comprendidos en los artículos 43, incisos 2 y 3, y 44, incisos 2, 3, 6 y 7. (*)

(*) Numeral modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

"2.- Los comprendidos en el artículo 44 numerales 6, 7 y 9."

3.- Los que carecen, en el momento de testar, por cualquier causa, aunque sea transitoria, de la lucidez mental y de la libertad necesarias para el otorgamiento de este acto.(*)

(*) Numeral derogado por el Literal a) de la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

Artículo 688 Nulidad de disposición testamentaria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son nulas las disposiciones testamentarias en favor del notario ante el cual se otorga el testamento, de su cónyuge o parientes dentro del cuarto grado de consanguinidad y segundo de afinidad, así como en favor de los testigos testamentarios.

Artículo 689 Aplicación de normas sobre modalidades de acto jurídico
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las normas generales sobre las modalidades de los actos jurídicos, se aplican a las disposiciones testamentarias; y se tienen por no puestos las condiciones y los cargos contrarios a las normas imperativas de la ley.

Artículo 690 Carácter personal y voluntario del acto testamentario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las disposiciones testamentarias deben ser la expresión directa de la voluntad del testador, quien no puede dar poder a otro para testar, ni dejar sus disposiciones al arbitrio de un tercero.

TITULO  II Formalidades de los Testamentos
==========================================

CAPITULO PRIMERO Disposiciones Comunes
######################################

Artículo 691 Tipos de testamento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los testamentos ordinarios son: el otorgado en escritura pública, el cerrado y el ológrafo. Los testamentos especiales, permitidos sólo en las circunstancias previstas en este título, son el militar y el marítimo.

CONCORDANCIAS:     Ley Nº 27261, Art. 74, inc. g) (Testamentos otorgados in extremis a bordo de aeronave)

Artículo 692 Formalidad del Testamento de analfabetos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los analfabetos pueden testar solamente en escritura pública, con las formalidades adicionales indicadas en el artículo 697.

Artículo 693 Formalidad del Testamento de ciegos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los ciegos pueden testar sólo por escritura pública, con las formalidades adicionales a que se refiere el artículo 697. (*)

(*) Artículo derogado por la Única Disposición Complementaria Derogatoria de la Ley N° 29973, publicada el 24 diciembre 2012.

Artículo 694 Formalidad de testamento de mudos, sordomudos y otros
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los mudos, los sordomudos y quienes se encuentren imposibilitados de hablar por cualquier otra causa, pueden otorgar sólo testamento cerrado u ológrafo. (*)

(*) Artículo derogado por la Única Disposición Complementaria Derogatoria de la Ley N° 29973, publicada el 24 diciembre 2012.

Artículo 695 Formalidades testamentarias
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las formalidades de todo testamento son la forma escrita, la fecha de su otorgamiento, el nombre del testador y su firma, salvo lo dispuesto en el artículo 697. Las formalidades específicas de cada clase de testamento no pueden ser aplicadas a los de otra.

CAPITULO SEGUNDO Testamento en Escritura Pública
################################################

Artículo 696 Formalidades del testamento por escritura pública
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las formalidades esenciales del testamento otorgado en escritura pública son:

1.- Que estén reunidos en un solo acto, desde el principio hasta el fin, el testador, el notario y dos testigos hábiles.

2.- Que el testador exprese por sí mismo su voluntad, dictando su testamento al notario o dándole personalmente por escrito las disposiciones que debe contener. (*)

(*) Numeral modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

"2.- Que el testador exprese por sí mismo su voluntad o, tratándose de una persona con discapacidad, con el otorgamiento de ajustes razonables o apoyos para la manifestación de voluntad, en caso lo requiera. Si así lo requiere, dictando su testamento al notario o dándole personalmente por escrito las disposiciones que debe contener."

3.- Que el notario escriba el testamento de su puño y letra, en su registro de escrituras públicas.

4.- Que cada una de las páginas del testamento sea firmada por el testador, los testigos y el notario.

5.- Que el testamento sea leído clara y distintamente por el notario, el testador o el testigo testamentario que éste elija.

6.- Que durante la lectura, al fin de cada cláusula, se verifique, viendo y oyendo al testador, si lo contenido en ella es la expresión de su voluntad.(*)

(*) Numeral modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 29973, publicada el 24 diciembre 2012, cuyo texto es el siguiente:

"6.- Que durante la lectura, al fin de cada cláusula, se verifique si el contenido corresponde a la expresión de su voluntad. Si el testador fuera una persona con discapacidad por deficiencia auditiva o de lenguaje, podrá expresar su asentimiento u observaciones directamente o a través de un intérprete." (*)

(*) Numeral modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

"6.- Que, durante la lectura, al fin de cada cláusula, se verifique si el contenido corresponde a la expresión de su voluntad. Si el testador fuera una persona con discapacidad, puede expresar su asentimiento u observaciones a través de ajustes razonables o apoyos en caso lo requiera."

7.- Que el notario deje constancia de las indicaciones que, luego de la lectura, pueda hacer el testador, y salve cualquier error en que se hubiera incurrido.

8.- Que el testador, los testigos y el notario firmen el testamento en el mismo acto.

"9.- Que, en los casos en que el apoyo de la persona con discapacidad sea un beneficiario, se requiere el consentimiento del juez.."(*)

(*) Numeral incorporado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018.

Artículo 697 Testador invidente, sordo e impedido de firmar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el testador es ciego o analfabeto, deberá leérsele el testamento dos veces, una por el notario y otra por el testigo testamentario que el testador designe. Si el testador es sordo el testamento será leído en alta voz por él mismo, en el registro del notario. Si el testador no sabe o no puede firmar lo hará a su ruego el testigo testamentario que él designe, de todo lo cual se hará mención en el testamento.(*)

(*) Artículo modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 29973, publicada el 24 diciembre 2012, cuyo texto es el siguiente:

"Artículo 697.- Testigo testamentario a ruego

Si el testador es analfabeto, deberá leérsele el testamento dos veces, una por el notario y otra por el testigo testamentario que el testador designe. Si el testador es una persona con discapacidad por deficiencia visual, el testamento podrá ser leído por él mismo utilizando alguna ayuda técnica o podrá leérselo el notario o el testigo testamentario que el testador designe. Si el testador es una persona con discapacidad por deficiencia auditiva o de lenguaje, el testamento será leído por él mismo en el registro del notario o con el apoyo de un intérprete. Si el testador no sabe o no puede firmar, lo hará a su ruego el testigo testamentario que él designe, de todo lo cual se hará mención en el testamento." (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

“Artículo 697.- Testigo testamentario a ruego

Si el testador es analfabeto, deberá leérsele el testamento dos veces, una por el notario y otra por el testigo testamentario que el testador designe. Si el testador no sabe o no puede firmar, lo hará a través del uso de la huella dactilar, de todo lo cual se mencionará en el testamento. En caso no tenga huella dactilar, el notario debe hacer uso de cualquier otro medio de verificación que permita acreditar la identidad del testador.”

Artículo 698 Suspensión de la facción de testamento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si se suspende la facción del testamento por cualquier causa, se hará constar esta circunstancia, firmando el testador, si puede hacerlo, los testigos y el notario. Para continuar el testamento deberán estar reunidos nuevamente el testador, el mismo notario y los testigos, si pueden ser habidos, u otros en caso distinto.

CAPITULO TERCERO Testamento Cerrado
###################################

Artículo 699 Formalidad del Testamento Cerrado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las formalidades esenciales del testamento cerrado son:

1.- Que el documento en que ha sido extendido esté firmado en cada una de sus páginas por el testador, bastando que lo haga al final si estuviera manuscrito por él mismo, y que sea colocado dentro de un sobre debidamente cerrado o de una cubierta clausurada, de manera que no pueda ser extraído el testamento sin rotura o alteración de la cubierta.(*)

(*) Numeral modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 29973, publicada el 24 diciembre 2012, cuyo texto es el siguiente:

"1.- Que el documento en que ha sido extendido esté firmado en cada una de sus páginas por el testador, bastando que lo haga al final si estuviera manuscrito por él mismo, y que sea colocado dentro de un sobre debidamente cerrado o de una cubierta clausurada, de manera que no pueda ser extraído el testamento sin rotura o alteración de la cubierta.

Tratándose de un testamento otorgado por una persona con discapacidad por deficiencia visual, podrá ser otorgado en sistema braille o utilizando algún otro medio o formato alternativo de comunicación, debiendo contar cada folio con la impresión de su huella dactilar y su firma, colocado dentro de un sobre en las condiciones que detalla el primer párrafo."

2.- Que el testador entregue personalmente al notario el referido documento cerrado, ante dos testigos hábiles, manifestándole que contiene su testamento. Si el testador es mudo o está imposibilitado de hablar, esta manifestación la hará por escrito en la cubierta.

3.- Que el notario extienda en la cubierta del testamento un acta en que conste su otorgamiento por el testador y su recepción por el notario, la cual firmarán el testador, los testigos y el notario, quien la transcribirá en su registro, firmándola las mismas personas.

4.- Que el cumplimiento de las formalidades indicadas en los incisos 2 y 3 se efectúe estando reunidos en un solo acto el testador, los testigos y el notario, quien dará al testador copia certificada del acta.

Artículo 700 Revocación del testamento cerrado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testamento cerrado quedará en poder del notario. El testador puede pedirle, en cualquier tiempo, la restitución de este testamento, lo que hará el notario ante dos testigos, extendiendo en su registro un acta en que conste la entrega, la que firmarán el testador, los testigos y el notario. Esta restitución produce la revocación del testamento cerrado, aunque el documento interno puede valer como testamento ológrafo si reúne los requisitos señalados en la primera parte del artículo 707.

Artículo 701 Custodia y presentación judicial de testamento cerrado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El notario bajo cuya custodia queda el testamento cerrado, lo conservará con las seguridades necesarias hasta que, después de muerto el testador, el juez competente, a solicitud de parte interesada que acredite la muerte del testador y la existencia del testamento, ordene al notario la presentación de este último. La resolución del juez competente se hará con citación de los presuntos herederos o legatarios.

Artículo 702 Apertura de testamento cerrado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Presentado el testamento cerrado, el juez, con citación de las personas indicadas en el artículo 701, procederá de conformidad con el Código de Procedimientos Civiles. (*)

(*)  La referencia al Código de Procedimientos Civiles debe entenderse efectuada al Código Procesal Civil.

Artículo 703 Modificación de testamento cerrado por ológrafo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el juez comprueba que la cubierta está deteriorada, de manera que haya sido posible el cambio del pliego que contiene el testamento, dispondrá que éste valga como ológrafo, si reúne los requisitos señalados en la primera parte del artículo 707.

CAPITULO CUARTO Impedimentos del Notario y de los Testigos Testamentarios
#########################################################################

Artículo 704 Impedimentos del notario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El notario que sea pariente del testador dentro del cuarto grado de consanguinidad o segundo de afinidad está impedido de intervenir en el otorgamiento del testamento por escritura pública o de autorizar el cerrado.

Artículo 705 Personas impedidas de ser testigos testamentarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Están impedidos de ser testigos testamentarios:

1.- Los que son incapaces de otorgar testamento.

2.- Los sordos, los ciegos y los mudos. (*)

(*) Numeral derogado por la Única Disposición Complementaria Derogatoria de la Ley N° 29973, publicada el 24 diciembre 2012.

3.- Los analfabetos.

4.- Los herederos y los legatarios en el testamento en que son instituidos y sus cónyuges, ascendientes, descendientes y hermanos.

5.- Los que tienen con el testador los vínculos de relación familiar indicados en el inciso anterior.

6.- Los acreedores del testador, cuando no pueden justificar su crédito sino con la declaración testamentaria.

7.- El cónyuge y los parientes del notario, dentro del cuarto grado de consanguinidad o segundo de afinidad, y los dependientes del notario o de otros notarios.

8.- Los cónyuges en un mismo testamento.

Artículo 706 Validez del testamento otorgado con testigo impedido
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Al testigo testamentario cuyo impedimento no fuera notorio al tiempo de su intervención, se le tiene como hábil si la opinión común así lo hubiera considerado.

CAPITULO QUINTO Testamento Ológrafo
###################################

Artículo 707 Formalidad del Testamento ológrafo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son formalidades esenciales del testamento ológrafo, que sea totalmente escrito, fechado y firmado por el propio testador.(*)

(*) Extremo modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 29973, publicada el 24 diciembre 2012, cuyo texto es el siguiente:

"Artículo 707.- Testamento ológrafo. Formalidades

Son formalidades esenciales del testamento ológrafo, que sea totalmente escrito, fechado y firmado por el propio testador. Si lo otorgara una persona con discapacidad por deficiencia visual, deberá cumplirse con lo expuesto en el segundo párrafo del numeral 1 del artículo 699."

Para que produzca efectos debe ser protocolizado, previa comprobación judicial, dentro del plazo máximo de un año contado desde la muerte del testador.

Artículo 708 Presentación de testamento ológrafo ante Juez
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La persona que conserve en su poder un testamento ológrafo, está obligada a presentarlo al juez competente dentro de los treinta días de tener conocimiento de la muerte del testador, bajo responsabilidad por el perjuicio que ocasione con su dilación, y no obstante lo dispuesto en la parte final del artículo 707.

Artículo 709 Apertura judicial de testamento ológrafo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Presentado el testamento ológrafo con la copia certificada de la partida de defunción del testador o declaración judicial de muerte presunta, el juez, con citación de los presuntos herederos, procederá a la apertura si estuviera cerrado, pondrá su firma entera y el sello del juzgado  en cada una de sus páginas y dispondrá lo necesario para la comprobación de la autenticidad de la letra y firma del testador mediante el cotejo, de conformidad con las disposiciones del Código de Procedimientos Civiles (*) que fueran aplicables.

Sólo en caso de faltar elementos para el cotejo, el juez puede disponer que la comprobación sea hecha por tres testigos que conozcan la letra y firma del testador.(*)

(*) Artículo modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 29973, publicada el 24 diciembre 2012, cuyo texto es el siguiente:

"Artículo 709.- Apertura judicial de testamento ológrafo

Presentado el testamento ológrafo con la copia certificada de la partida de defunción del testador o declaración judicial de muerte presunta, el juez, con citación a los presuntos herederos, procederá a la apertura si estuviera cerrado, pondrá su firma entera y el sello del juzgado en cada una de sus páginas y dispondrá lo necesario para la comprobación de la autenticidad de la letra y firma del testador mediante el cotejo, de conformidad con las disposiciones del Código Procesal Civil que fueran aplicables.

Solo en caso de faltar elementos para el cotejo, el juez puede disponer que la comprobación sea hecha por tres testigos que conozcan la letra y firma del testador.

En caso de un testamento otorgado en sistema braille u otro medio o formato alternativo de comunicación, la comprobación se hará sobre la firma y huella digital del testador."

(*)  La referencia al Código de Procedimientos Civiles debe entenderse efectuada al Código Procesal Civil.

Artículo 710 Traducción oficial de testamento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el testamento estuviera escrito en idioma distinto del castellano, el juez nombrará un traductor oficial. Además, si el testador fuera extranjero, la traducción será hecha con citación del cónsul del país de su nacionalidad, si lo hubiera. La versión será agregada al texto original, suscrita por el traductor con su firma legalizada por el secretario del juzgado. El juez autenticará también este documento con su firma entera y con el sello del juzgado.(*)

(*) Extremo modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 29973, publicada el 24 diciembre 2012, cuyo texto es el siguiente:

"Artículo 710.- Traducción oficial de testamento

Si el testamento estuviera escrito en idioma distinto del castellano, el juez nombrará un traductor oficial. Además, si el testador fuera extranjero, la traducción será hecha con citación del cónsul del país de su nacionalidad, si lo hubiera. Igualmente, el juez podrá nombrar un traductor si el testamento hubiera sido otorgado en sistema braille u otro medio o formato alternativo de comunicación. La versión será agregada al texto original, suscrita por el traductor con su firma legalizada por el secretario del juzgado. El juez autenticará también este documento con su firma entera y con el sello del juzgado."

Esta disposición es aplicable también en la comprobación del testamento cerrado.

Artículo 711 Protocolización del expediente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Comprobada la autenticidad del testamento y el cumplimiento de sus requisitos de forma, el juez mandará protocolizar el expediente.

CAPITULO SEXTO Testamento Militar
#################################

Artículo 712 Testamento militar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pueden otorgar testamento militar los miembros de las Fuerzas Armadas y de las Fuerzas Policiales, que en tiempo de guerra estén dentro o fuera del país, acuartelados o participando en operaciones bélicas; las personas que sirvan o sigan a dichas fuerzas; y los prisioneros de guerra que estén en poder de las mismas.

Los prisioneros que se encuentren en poder del enemigo tienen el mismo derecho, conforme a las Convenciones Internacionales.

Artículo 713 Personas ante quienes se puede otorgar testamento militar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testamento militar puede ser otorgado ante un oficial, o ante el jefe del destacamento, puesto o comando al que pertenezca el testador, aunque dicho jefe no tenga la clase de oficial, o ante el médico o el capellán que lo asistan, si el testador está herido o enfermo, y en presencia de dos testigos.

Son formalidades de este testamento que conste por escrito y que sea firmado por el testador, por la persona ante la cual es otorgado y por los testigos.

Artículo 714 Trámite del testamento militar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testamento militar se hará llegar, a la brevedad posible y por conducto regular, al respectivo Cuartel General, donde se dejará constancia de la clase militar o mando de la persona ante la cual ha sido otorgado. Luego será remitido al Ministerio al que corresponda, que lo enviará al juez de primera instancia de la capital de la provincia donde el testador tuvo su último domicilio.

Si en las prendas de algunas de las personas a que se refiere el artículo 712 y que hubiera muerto, se hallara un testamento ológrafo, se le dará el mismo trámite.

Artículo 715 Caducidad del testamento militar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testamento militar caduca a los tres meses desde que el testador deje de estar en campaña y llegue a un lugar del territorio nacional donde sea posible otorgar testamento en las formas ordinarias.

El plazo de caducidad se computa a partir de la fecha del documento oficial que autoriza el retorno del testador, sin perjuicio del término de la distancia.

Si el testador muere antes del plazo señalado para la caducidad, sus presuntos herederos o legatarios pedirán ante el juez en cuyo poder se encuentra el testamento, su comprobación judicial y protocolización notarial, conforme a las disposiciones de los artículos 707, segundo párrafo, a 711.

Si el testamento otorgado en las circunstancias a que se refiere el artículo 712 tuviera los requisitos del testamento ológrafo, caduca al año de la muerte del testador.

CAPITULO SEPTIMO Testamento Marítimo
####################################

Artículo 716 Personas que pueden otorgar testamento marítimo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pueden otorgar testamento, durante la navegación acuática, los jefes, oficiales, tripulantes y cualquier otra persona que se encuentre embarcada en un buque de guerra peruano.

El mismo derecho tienen durante la navegación, los oficiales, tripulantes, pasajeros y cualquier otra persona que se encuentre a bordo de un barco mercante de bandera peruana, de travesía o de cabotaje, o que esté dedicado a faenas industriales o a fines científicos.

Artículo 717 Formalidades del testamento marítimo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testamento marítimo será otorgado ante quien tenga el mando del buque o ante el oficial en quien éste delegue la función y en presencia de dos testigos. El testamento del comandante del buque de guerra o del capitán del barco mercante será otorgado ante quien le siga en el mando.

Son formalidades de este testamento que conste por escrito y que sea firmado por el testador, por la persona ante la cual es otorgado y por los testigos. Se extenderá, además, un duplicado con las mismas firmas que el original.

El testamento será anotado en el diario de bitácora, de lo cual se dejará constancia en ambos ejemplares con el visto bueno de quien ejerce el mando de la nave, y se conservará con los documentos de éste.

Artículo 718 Protección del testamento marítimo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si antes de regresar al Perú la nave arriba a un puerto extranjero donde hubiera agente consular, el comandante o capitán de la nave le entregará, bajo cargo, uno de los ejemplares del testamento. El referido agente lo remitirá al Ministerio de Marina, si el testamento hubiere sido otorgado en un buque de guerra, o a la Dirección General de Capitanías, si fue otorgado en un barco mercante, para los fines a que se refiere el artículo 719. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 719 Trámite del testamento marítimo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Al retorno de la nave al Perú los dos ejemplares o el ejemplar restante en el caso del artículo 718, serán entregados al Ministerio de Marina, si el buque es de guerra; o a la Capitanía del Puerto de destino para su remisión a la Dirección General de Capitanías, si el barco es mercante. En uno u otro caso, la autoridad respectiva enviará un ejemplar al juez de primera instancia de la provincia donde el testador tuvo su último domicilio y archivará el otro. Si el testador fuere extranjero y no estuviera domiciliado en el Perú, un ejemplar será remitido al Ministerio de Relaciones Exteriores.

En caso de muerte del testador durante el viaje, se agregará a cada ejemplar una copia certificada del acta que acredite la defunción. En igual caso, si se encuentra entre las prendas del difunto un testamento ológrafo, éste será guardado con los papeles de la nave, agregándosele copia certificada del acta que acredite la defunción y se le dará el mismo curso indicado en el párrafo anterior.

Artículo 720 Caducidad del testamento marítimo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testamento marítimo caduca a los tres meses de haber desembarcado definitivamente el testador. Si muere antes del vencimiento de este plazo, sus presuntos herederos o legatarios, pedirán al juez cuyo poder se encuentre, su comprobación judicial y protocolización notarial, conforme a las disposiciones de los artículos 707, segundo párrafo, a 711.

Si el testamento otorgado a las circunstancias a que se refiere el artículo 716 tuviera los requisitos del testamento ológrafo, caduca al año de la muerte del testador.

CAPITULO OCTAVO Testamentos otorgados en el Extranjero
######################################################

Artículo 721 Formalidad del Testamento otorgado en el extranjero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los peruanos que residen o se hallen en el extranjero pueden otorgar testamento ante el agente consular del Perú, por escritura pública o cerrado, según lo dispuesto en los artículos 696 a 703, respectivamente. En estos casos aquél cumplirá la función de notario público.

Puede también otorgar testamento ológrafo, que será válido en el Perú, aunque la ley del respectivo país no admita esta clase de testamento.

Artículo 722 Válidez de testamento otorgado en el extranjero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son válidos en el Perú en cuanto a su forma, los testamentos otorgados en otro país por los peruanos o los extranjeros, ante los funcionarios autorizados para ello y según las formalidades establecidas por la ley del respectivo país, salvo los testamentos mancomunado y verbal y las modalidades testamentarias incompatibles con la ley peruana.

TITULO  III La Legítima y la Porción Disponible
===============================================

CAPITULO UNICO
##############

Artículo 723 Noción de legítima
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La legítima constituye la parte de la herencia de la que no puede disponer libremente el testador cuando tiene herederos forzosos.

Artículo 724 Herederos forzosos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son herederos forzosos los hijos y los demás descendientes, los padres y los demás ascendientes, y el cónyuge. (*)

(*) Artículo modificado por el Artículo 5 de la Ley N° 30007, publicada el 17 abril 2013, cuyo texto es el siguiente:

“Artículo 724.- Herederos forzosos

Son herederos forzosos los hijos y los demás descendientes, los padres y los demás ascendientes, el cónyuge o, en su caso, el integrante sobreviviente de la unión de hecho.”

Artículo 725 Tercio de libre disposición
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que tiene hijos u otros descendientes, o cónyuge, puede disponer libremente hasta del tercio de sus bienes.

Artículo 726 Libre disposición de la mitad de los bienes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que tiene sólo padres u otros ascendientes, puede disponer libremente hasta de la mitad de sus bienes.

Artículo 727 Libre disposición de la totalidad de los bienes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que no tiene cónyuge ni parientes de los indicados en los artículos 725 y 726, tiene la libre disposición de la totalidad de sus bienes.

Artículo 728 Gravamen sobre la porción disponible
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el testador estuviese obligado al pago de una pensión alimenticia conforme al artículo 415, la porción disponible quedará gravada hasta donde fuera necesario para cumplirla.

Artículo 729 Legítima de heredero forzoso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La legítima de cada uno de los herederos forzosos es una cuota igual a la que les corresponde en la sucesión intestada, cuyas disposiciones rigen, asimismo, su concurrencia, participación o exclusión.

Artículo 730 Legítima del cónyuge
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La legítima del cónyuge es independiente del derecho que le corresponde por concepto de gananciales provenientes de la liquidación de la sociedad de bienes del matrimonio.

Artículo 731 Derecho de habitación vitalicia del cónyuge supérstite
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el cónyuge sobreviviente concurra con otros herederos y sus derechos por concepto de legítima y gananciales no alcanzaren el valor necesario para que le sea adjudicada la casa-habitación en que existió el hogar conyugal, dicho cónyuge podrá optar por el derecho de habitación en forma vitalicia y gratuita sobre la referida casa. Este derecho recae sobre la diferencia existente entre el valor del bien y el de sus derechos por concepto de legítima y gananciales.

La diferencia de valor afectará la cuota de libre disposición del causante y, si fuere necesario, la reservada a los demás herederos en proporción a los derechos hereditarios de éstos.

En su caso, los otros bienes se dividen entre los demás herederos, con exclusión del cónyuge sobreviviente.

Artículo 732 Derecho de usufructo del cónyuge supérstite
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si en el caso del artículo 731 el cónyuge sobreviviente no estuviere en situación económica que le permita sostener los gastos de la casa-habitación, podrá, con autorización judicial, darla en arrendamiento, percibir para sí la renta y ejercer sobre la diferencia existente entre el valor del bien y el de sus derechos por concepto de legítima y gananciales los demás derechos inherentes al usufructuario. Si se extingue el arrendamiento, el cónyuge sobreviviente podrá readquirir a su sola voluntad el derecho de habitación a que se refiere el artículo 731.

Mientras esté afectado por los derechos de habitación o de usufructo, en su caso, la casa-habitación tendrá la condición legal de patrimonio familiar.

Si el cónyuge sobreviviente contrae nuevo matrimonio, vive en concubinato o muere, los derechos que le son concedidos en este artículo y en el artículo 731 se extinguen, quedando expedita la partición del bien. También se extinguen tales derechos cuando el cónyuge sobreviviente renuncia a ellos.

Artículo 733 Intangibilidad de la legítima
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testador no puede privar de la legítima a sus herederos forzosos, sino en los casos expresamente determinados por la ley, ni imponer sobre aquélla gravamen, modalidad, ni sustitución alguna. Tampoco puede privar a su cónyuge de los derechos que le conceden los artículos 731 y 732, salvo en los referidos casos.

TITULO  IV Institución y Sustitución de Herederos y Legatarios
==============================================================

CAPITULO UNICO
##############

Artículo 734 Institución de heredero o legatario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La institución de heredero o legatario debe recaer en persona cierta, designada de manera indubitable por el testador, salvo lo dispuesto en el artículo 763, y ser hecha sólo en testamento.

Artículo 735 Sucesión a título universal y particular
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La institución de heredero es a título universal y comprende la totalidad de los bienes, derechos y obligaciones que constituyen la herencia o una cuota parte de ellos. La institución de legatario es a título particular y se limita a determinados bienes, salvo lo dispuesto en el artículo 756. El error del testador en la denominación de uno u otro no modifica la naturaleza de la disposición.

Artículo 736 Forma de institución de heredero forzoso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La institución de heredero forzoso se hará en forma simple y absoluta. Las modalidades que imponga el testador se tendrán por no puestas.

Artículo 737 Institución de heredero voluntario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testador que no tenga herederos forzosos, puede instituir uno o más herederos voluntarios y señalar la parte de la herencia que asigna a cada uno. Si no la determina, sucederán en partes iguales.

Artículo 738 Caudal disponible para legatarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testador puede instituir legatarios, con la parte disponible si tiene herederos forzosos, y no teniéndolos, hasta con la totalidad de sus bienes y señalar los que asigna a cada uno de los legatarios.

El testador puede imponer tanto a los herederos voluntarios como a los legatarios, condiciones y cargos que no sean contrarios a la ley, a las buenas costumbres y al libre ejercicio de los derechos fundamentales de la persona.

Artículo 739 Remanente que corresponde a herederos legales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el testador que carece de herederos forzosos no ha instituido herederos voluntarios y dispone en legados de sólo parte de sus bienes, el remanente que hubiere corresponde a sus herederos legales.

Artículo 740 Igualdad de condiciones y cargos entre sustitutos y legatarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testador puede designar sustituto a los herederos voluntarios y a los legatarios para el caso en que el instituido muera antes que el testador, o que renuncie a la herencia o al legado o que los pierda por indignidad.

Artículo 741 Igualdad de condiciones y cargos entre sustitutos e instituidos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los herederos voluntarios y legatarios sustitutos quedan sujetos a las mismas condiciones y cargos que el instituido, a menos que el testador disponga otra cosa, o que las condiciones y cargos impuestos sean por su naturaleza inherentes a la persona del instituido.

TITULO  V Desheredación
=======================

CAPITULO UNICO
##############

Artículo 742 Noción de desheredación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Por la desheredación el testador puede privar de la legítima al heredero forzoso que hubiera incurrido en alguna de las causales previstas en la ley.

Artículo 743 Obligación de expresar causal de desheredación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La causal de desheredación debe ser expresada claramente en el testamento. La desheredación dispuesta sin expresión de causa, o por causa no señalada en la ley, o sujeta a condición, no es válida. La fundada en causa falsa es anulable.

Artículo 744 Causales de desheredación de descendientes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son causales de desheredación de los descendientes:

1.- Haber maltratado de obra o injuriado grave y reiteradamente al ascendiente o a su cónyuge, si éste es también ascendiente del ofensor.

2.- Haberle negado sin motivo justificado los alimentos o haber abandonado al ascendiente encontrándose éste gravemente enfermo o sin poder valerse por sí mismo.

3.- Haberle privado de su libertad injustificadamente.

4.- Llevar el descendiente una vida deshonrosa o inmoral.

Artículo 745 Causales de desheredación de ascendientes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son causales de desheredación de los ascendientes:

1.- Haber negado injustificadamente los alimentos a sus descendientes.

2.- Haber incurrido el ascendiente en alguna de las causas por las que se pierde la patria potestad o haber sido privado de ella.

Artículo 746 Causales de desheredación del cónyuge
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son causales de desheredación del cónyuge las previstas en el artículo 333, incisos 1 a 6. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 747 Desheredación por indignidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testador puede fundamentar la desheredación en las causales específicas de ésta, enumeradas en los artículos 744 a 746, y en las de indignidad señaladas en el artículo 667.

Artículo 748 Personas exentas de desheredación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No pueden ser desheredados los incapaces menores de edad, ni los mayores que por cualquier causa se encuentren privados de discernimiento. Estas personas tampoco pueden ser excluidas de la herencia por indignidad.

Artículo 749 Efectos de desheredación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los efectos de la desheredación se refieren a la legítima y no se extienden a las donaciones y legados otorgados al heredero, que el causante puede revocar, ni a los alimentos debidos por ley, ni a otros derechos que corresponden al heredero con motivo de la muerte del testador.

Artículo 750 Derecho de contradecir la desheredación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El derecho de contradecir la desheredación corresponde al desheredado o a sus sucesores y se extingue a los dos años, contados desde la muerte del testador o desde que el desheredado tiene conocimiento del contenido del testamento.

Artículo 751 Acción del causante para justificar desheredación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que deshereda puede promover juicio para justificar su decisión.

La sentencia que se pronuncie impide contradecir la desheredación. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

Acción del causante para justificar desheredación

"Artículo 751.-  El que deshereda puede interponer demanda contra el desheredado para justificar su decisión. La demanda se tramita como proceso abreviado. La sentencia que se pronuncie impide contradecir la desheredación."

Artículo 752 Prueba de desheredación a cargo de herederos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En caso de no haberse promovido juicio por el testador para justificar la desheredación, corresponde a sus herederos probar la causa, si el desheredado o sus sucesores la contradicen.

Artículo 753 Revocación de la desheredación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La desheredación queda revocada por instituir heredero al desheredado o por declaración expresada en el testamento o en escritura pública. En tal caso, no produce efecto el juicio anterior seguido para justificar la desheredación.

Artículo 754 Renovación de desheredación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Revocada la desheredación no puede ser renovada sino por hechos posteriores.

Artículo 755 Herederos en representación de desheredado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los descendientes del desheredado heredan por representación la legítima que correspondería a éste si no hubiere sido excluido. El desheredado no tiene derecho al usufructo ni a la administración de los bienes que por esta causa adquieran sus descendientes que sean menores de edad o incapaces.

TITULO  VI Legados
==================

CAPITULO UNICO
##############

Artículo 756 Facultad de disponer por legado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testador puede disponer como acto de liberalidad y a título de legado, de uno o más de sus bienes, o de una parte de ellos, dentro de su facultad de libre disposición.

Artículo 757 Invalidez del legado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No es válido el legado de un bien determinado, si no se halla en el dominio del testador al tiempo de su muerte.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 758 Legado de bien indeterminado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es válido el legado de un bien mueble indeterminado, aunque no lo haya en la herencia. La elección, salvo disposición diversa del testador, corresponde al encargado de pagar el legado, quien cumplirá con dar un bien que no sea de calidad inferior ni superior a aquél, debiendo tener en consideración la parte disponible de la herencia y las necesidades del legatario.

Artículo 759 Legado de bien parcialmente ajeno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El legado de un bien que pertenece al testador sólo en parte o sobre el cual éste tiene otro derecho, es válido en cuanto a la parte o al derecho que corresponde al testador.

Artículo 760 Legado de bien gravado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el testador lega un bien que está gravado por derechos reales de garantía, el bien pasará al legatario con los gravámenes que tuviere. El servicio de amortización e intereses de la deuda, serán de cargo del testador hasta el día de su muerte.

Artículo 761 Legado de bien sujeto a uso, usufructo y habitación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el bien legado estuviere sujeto a usufructo, uso o habitación en favor de tercera persona, el legatario respetará  estos derechos hasta que se extingan .

Artículo 762 Legado de crédito y condonación de deuda
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El legado de un crédito tiene efecto sólo en cuanto a la parte del mismo que subsiste en el momento de la muerte del testador. El heredero está obligado a entregar al legatario el título del crédito que le ha sido legado. El legado de liberación de una deuda comprende lo adeudado a la fecha de apertura de la sucesión. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 763 Legado para fines sociales, culturales y religiosos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son válidos los legados hechos en favor de los pobres o para fines culturales o religiosos, que serán entregados por el heredero a quienes indique el testador. A falta de indicación los primeros serán entregados a la Beneficencia Pública; los segundos al Instituto Nacional de Cultura o a los organismos que hagan sus veces en uno u otro caso; y los terceros, a la autoridad competente de la religión que profesaba el testador.

Artículo 764 Legado de predio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el bien legado es un predio, los terrenos y las nuevas construcciones que el testador haya agregado después del testamento no forman parte del legado, salvo las mejoras introducidas en el inmueble, cualquiera que fuese su clase.

Artículo 765 Legado en dinero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El legado en dinero debe ser pagado en esta especie, aunque no lo haya en la herencia.

Artículo 766 Legado de alimentos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El legado de alimentos, si el testador no determinó su cuantía y forma de pago, se cumple asignando al legatario una pensión que se regirá por lo establecido en las disposiciones de los artículos 472 a 487.

Artículo 767 Legado remuneratorio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El legado remuneratorio se considera como pago, en la parte en que corresponda razonablemente al servicio prestado por el beneficiario del testador y como acto de liberalidad en cuanto al exceso.

Artículo 768 Legado sujeto a modalidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El legatario no adquiere el legado subordinado a condición suspensiva o al vencimiento de un plazo, mientras no se cumpla la condición o venza el plazo. Mientras tanto puede ejercer las medidas precautorias de su derecho. El legado con cargo, se rige por lo dispuesto para las donaciones sujetas a esta modalidad.

Artículo 769 Legado de bien determinado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el legado de bien determinado no sujeto a condición o plazo, el legatario lo adquiere en el estado en que se halle a la muerte del testador. Desde ese momento le corresponden los frutos del bien legado y asume el riesgo de su pérdida o deterioro, salvo dolo o culpa de quien lo tuviere en su poder.

Artículo 770 Reducción del legado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el valor de los legados excede de la parte disponible de la herencia, éstos se reducen a prorrata, a menos que el testador haya establecido el orden en que deben ser pagados.

El legado hecho en favor de alguno de los coherederos no está sujeto a reducción, salvo que la herencia fuere insuficiente para el pago de las deudas.

Artículo 771 Cuarta falcidia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el testador que tiene la libre disposición de sus bienes instituye herederos voluntarios y legatarios, la parte que corresponde a aquéllos no será menor de la cuarta parte de la herencia, con cuyo objeto serán reducidos a prorrata los legados, si fuere necesario.

Artículo 772 Caducidad del legado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Caduca el legado:

1.- Si el legatario muere antes que el testador.

2.- Si el legatario se divorcia o se separa judicialmente del testador por su culpa.

3.- Si el testador enajena el bien legado o éste perece sin culpa del heredero.

CONCORDANCIAS:     R. Nº 156-2012-SUNARP-SN, Art. 18

Artículo 773 Aceptación y renuncia del legado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es aplicable al legado la disposición del artículo 677.

TITULO  VII Derecho de Acrecer
==============================

CAPITULO UNICO
##############

Artículo 774 Derecho de acrecer entre coherederos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si varios herederos son instituidos en la totalidad de los bienes sin determinación de partes o en partes iguales y alguno de ellos no quiere o no puede recibir la suya, ésta acrece las de los demás, salvo el derecho de representación.

Artículo 775 Derecho de acrecer entre colegatarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando un mismo bien es legado a varias personas, sin determinación de partes y alguna de ellas no quiera o no pueda recibir la que le corresponde, ésta acrecerá las partes de los demás.

Artículo 776 Reintegro del legado a la masa hereditaria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El legado se reintegra a la masa hereditaria cuando no tiene efecto por cualquier causa, o cuando el legatario no puede o no quiere recibirlo.

Artículo 777 Improcedencia del derecho a acrecer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El derecho de acrecer no tiene lugar cuando del testamento resulta una voluntad diversa del testador.

TITULO  VIII Albaceas
=====================

CAPITULO UNICO
##############

CONCORDANCIAS:     D.S. N° 052-2008-PCM, Art. 6 (De la firma digital)

Artículo 778 Nombramiento de albacea
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testador puede encomendar a una o varias personas, a quienes se denomina albaceas o ejecutores testamentarios, el cumplimiento de sus disposiciones de última voluntad.

Artículo 779 Formalidad del nombramiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El nombramiento de albacea debe constar en testamento.

Artículo 780 Pluralidad de albaceas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando hay varios albaceas testamentarios nombrados para que ejerzan el cargo conjuntamente, vale lo que todos hagan de consuno o lo que haga uno de ellos autorizado por los demás. En caso de desacuerdo vale lo que decide la mayoría.

Artículo 781 Responsabilidad solidaria de los albaceas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es solidaria la reponsabilidad de los albaceas que ejercen conjuntamente el cargo, salvo disposición distinta del testador.

Artículo 782 Ejercicio concurrente o sucesivo del albacea
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el testador no dispone que los albaceas actúen conjuntamente, ni les atribuye funciones específicas a cada uno de ellos, desempeñarán el cargo sucesivamente, unos a falta de otros, en el orden en que se les ha designado.

Artículo 783 Personas impedidas para ser albaceas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No puede ser albacea el que está incurso en los artículos 667, 744, 745 y 746.

Artículo 784 Albaceazgo por personas jurídicas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pueden ser albaceas las personas jurídicas autorizadas por ley o por su estatuto.

Artículo 785 Excusa y renuncia del albacea
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El albacea puede excusarse de aceptar el cargo, pero si lo hubiera aceptado, no podrá renunciarlo sino por justa causa, a juicio del juez.

Artículo 786 Plazo para aceptación del cargo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mientras el albacea no acepte el cargo o no se excuse, el juez al que corresponda conocer de la sucesión, a solicitud de parte interesada, le señalará un plazo prudencial para la aceptación, transcurrido el cual se tendrá por rehusado.

Artículo 787 Obligaciones del albacea
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Son obligaciones del albacea:

1.- Atender a la inhumación del cadáver del testador o a su incineración si éste lo hubiera dispuesto así, sin perjuicio de lo establecido en el artículo 13.

2.- Ejercitar las acciones judiciales y extrajudiciales para la seguridad de los bienes hereditarios.

3.- Hacer inventario judicial de los bienes que constituyen la herencia, con citación de los herederos, legatarios y acreedores de quienes tenga conocimiento.

4.- Administrar los bienes de la herencia que no hayan sido adjudicados por el testador, hasta que sean entregados a los herederos o legatarios, salvo disposición diversa del testador.

5.- Pagar las deudas y cargas de la herencia, con conocimiento de los herederos.

6.- Pagar o entregar los legados.

7.- Vender los bienes hereditarios con autorización expresa del testador, o de los herederos, o del juez, en cuanto sea indispensable para pagar las deudas de la herencia y los legados.

8.- Procurar la división y partición de la herencia.

9.- Cumplir los encargos especiales del testador.

10.- Sostener la validez del testamento en el juicio de impugnación que se promueva, sin perjuicio del apersonamiento que, en tal caso, corresponde a los herederos.

Artículo 788 Personería específica de los albaceas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los albaceas no son representantes de la testamentaría para demandar ni responder en juicio, sino tratándose de los encargos del testador, de la administración que les corresponde y del caso del artículo 787, inciso 10.

Artículo 789 Carácter personal del albaceazgo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El albaceazgo es indelegable; pero pueden ejercerse en casos justificados algunas funciones mediante representantes, bajo las órdenes y responsabilidad del albacea.

Artículo 790 Posesión de bienes por el albacea
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el testador no instituye herederos, sino solamente legatarios, la posesión de los bienes hereditarios corresponde al albacea, hasta que sean pagadas las deudas de la herencia y los legados.

Artículo 791 Actos de conservación del albacea
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los herederos o legatarios pueden pedir al albacea la adopción de medidas necesarias para mantener la indemnidad de los bienes hereditarios.

Artículo 792 Albacea dativo
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el testador no hubiera designado albacea o si el nombrado no puede o no quiere desempeñar el cargo, sus atribuciones serán ejercidas por los herederos, y si no están de acuerdo, deberán pedir al juez el nombramiento de albacea dativo.

Artículo 793 Remuneración del albacea
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El cargo de albacea es remunerado, salvo que el testador disponga su gratuidad.

La remuneración no será mayor del cuatro por ciento de la masa líquida.

En defecto de la determinación de la remuneración por el testador, lo hará el juez, quien también señalará la del albacea dativo.

Artículo 794 Rendición de cuenta del albacea
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El albacea dará cuenta documentada del albaceazgo inmediatamente después de haberlo ejercido, a los herederos o a los legatarios si sólo hubiera éstos, aunque el testador lo exima de esta obligación.

La dará también durante el ejercicio del cargo, cuando lo ordene el juez, a petición de parte interesada. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

Rendición de cuenta del albacea

"Artículo 794.-  Aunque el testador le hubiera eximido de este deber, dentro de los sesenta días de terminado el albaceazgo, el albacea debe presentar a los sucesores un informe escrito de su gestión y, de ser el caso, las cuentas correspondientes, con los documentos del caso u ofreciendo otro medio probatorio. Las cuentas no requieren la observancia de formalidad especial en cuanto a su contenido, siempre que figure una relación ordenada de ingresos y gastos.

También cumplirá este deber durante el ejercicio del cargo, con frecuencia no inferior a seis meses, cuando lo ordene el Juez Civil a pedido de cualquier sucesor. La solicitud se tramita como proceso no contencioso.

El informe y las cuentas se entienden aprobados si dentro del plazo de caducidad de sesenta días de presentados no se solicita judicialmente su desaprobación, como proceso de conocimiento.

Las reglas contenidas en este artículo son de aplicación supletoria a todos los demás casos en los que exista deber legal o convencional de presentar cuentas de ingresos y gastos o informes de gestión."

Artículo 795 Remoción del albacea
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Deja de ser albacea el que no empieza la facción de inventarios dentro de los noventa días contados desde la muerte del testador o dentro de los treinta días de haber sido requerido para ello, notarial o judicialmente. (*)

(*) Artículo modificado por la Primera Disposición Modificatoria del Texto Unico Ordenado del Código Procesal Civil, aprobado por Resolución Ministerial Nº 010-93-JUS, publicada el 22 abril 1993, cuyo texto es el siguiente:

Remoción del albacea

"Artículo 795.-  Puede solicitarse, como proceso sumarísimo, la remoción del albacea que no ha empezado la facción de inventarios dentro de los noventa días de la muerte del testador, o de protocolizado el testamento, o de su nombramiento judicial, lo que corresponda, o dentro de los treinta días de haber sido requerido notarialmente con tal objeto por los sucesores."

Artículo 796 Cese del cargo del albacea
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El cargo de albacea termina:

1.- Por haber transcurrido dos años desde su aceptación, salvo el mayor plazo que señale el testador, o que conceda el juez con acuerdo de la mayoría de los herederos.

2.- Por haber concluido sus funciones.

3.- Por renuncia con aprobación judicial.

4.- Por incapacidad legal o física que impida el desempeño de la función.

5.- Por remoción judicial, a petición de parte debidamente fundamentada.

6.- Por muerte, desaparición o declaración de ausencia.

Artículo 797 Obligación de albacea de cumplir con la voluntad del testador
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El albacea está facultado durante el ejercicio de su cargo y en cualquier tiempo después de haberlo ejercido, para exigir que se cumpla la voluntad del testador. Carece de esta facultad el que cesó por renuncia o por haber sido removido del cargo.

TITULO  IX Revocación, Caducidad y Nulidad de los Testamentos
=============================================================

CAPITULO PRIMERO Revocación
###########################

Artículo 798 Revocación del testamento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testador tiene el derecho de revocar, en cualquier tiempo, sus disposiciones testamentarias. Toda declaración que haga en contrario carece de valor.

Artículo 799 Forma de revocar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La revocación expresa del testamento, total o parcial, o de algunas de sus disposiciones, sólo puede ser hecha por otro testamento, cualquiera que sea su forma.

Artículo 800 Reviviscencia de testamento anterior
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el testamento que revoca uno anterior es revocado a su vez por otro posterior, reviven las disposiciones del primero, a menos que el testador exprese su voluntad contraria.

Artículo 801 Revocación parcial de testamento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testamento que no es revocado total y expresamente por otro posterior, subsiste en las disposiciones compatibles con las de este último.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 802 Revocación del testamento cerrado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testamento cerrado queda revocado si el testador lo retira de la custodia del notario.

Artículo 803 Validez del testamento cerrado como ológrafo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tanto en el caso previsto en el artículo 802 como en el de su apertura por el testador, el testamento cerrado vale como ológrafo si se conserva el pliego interior y éste reúne las formalidades señaladas en la primera parte del artículo 707.

Artículo 804 Revocación de testamento ológrafo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testamento ológrafo queda revocado si el testador lo rompe, destruye o inutiliza de cualquier otra manera.

CAPITULO SEGUNDO Caducidad
##########################

Artículo 805 Caducidad de testamento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testamento caduca, en cuanto a la institución de heredero:

1.- Si el testador deja herederos forzosos que no tenía cuando otorgó el testamento y que vivan; o que estén concebidos al momento de su muerte, a condición de que nazcan vivos.

2.- Si el heredero renuncia a la herencia o muere antes que el testador sin dejar representación sucesoria, o cuando el heredero es el cónyuge y se declara la separación judicial por culpa propia o el divorcio.

3.- Si el heredero pierde la herencia por declaración de indignidad o por desheredación, sin dejar descendientes que puedan representarlo.

CONCORDANCIAS:     R. Nº 156-2012-SUNARP-SN, Art. 18 (Título para la inscripción de la caducidad de la institución de legatario o heredero)

Artículo 806 Preterición de heredero forzoso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La preterición de uno o más herederos forzosos, invalida la institución de herederos en cuanto resulte afectada la legítima que corresponde a los preteridos. Luego de haber sido pagada ésta, la porción disponible pertenece a quienes hubieren sido instituidos indebidamente herederos, cuya condición legal es la de legatarios.

Artículo 807 Reducción de disposiciones testamentarias
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las disposiciones testamentarias que menoscaban la legítima de los herederos, se reducirán, a petición de éstos, en lo que fueren excesivas.

CAPITULO TERCERO Nulidad
########################

Artículo 808 Nulidad y anulabilidad de testamento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es nulo el testamento otorgado por incapaces menores de edad y por los mayores enfermos mentales cuya interdicción ha sido declarada. Es anulable el de las demás personas incapaces comprendidas en el artículo 687. (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1384, publicado el 04 septiembre 2018, cuyo texto es el siguiente:

“Artículo 808.- Nulidad y anulabilidad de testamento

Es nulo el testamento otorgado por menores de edad. Es anulable el de las demás personas comprendidas en el artículo 687.”

Artículo 809 Anulabilidad de testamento por vicios de voluntad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es anulable el testamento obtenido por la violencia, la intimidación o el dolo. También son anulables las disposiciones testamentarias debidas a error esencial de hecho o de derecho del testador, cuando el error aparece en el testamento y es el único motivo que ha determinado al testador a disponer.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 810 Nulidad por falsa muerte de heredero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando un testamento ha sido otorgado expresando como causa la muerte del heredero instituido en uno anterior, valdrá éste y se tendrá por no otorgado aquél, si resulta falsa la noticia de la muerte.

Artículo 811 Nulidad por defectos de forma
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testamento es nulo de pleno derecho, por defectos de forma, si es infractorio de lo dispuesto en el artículo 695 o, en su caso, de los artículos 696, 699 y 707, salvo lo previsto en el artículo 697.

Artículo 812 Anulabilidad por defectos de forma
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testamento es anulable por defectos de forma cuando no han sido cumplidas las demás formalidades señaladas para la clase de testamento empleada por el testador. La acción no puede ser ejercida en este caso por quienes ejecutaron voluntariamente el testamento, y caduca a los dos años contados desde la fecha en que el heredero tuvo conocimiento del mismo.

Artículo 813 Nulidad y anulabilidad de testamentos especiales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los testamentos especiales son nulos de pleno derecho cuando falta la forma escrita, la firma del testador o de la persona autorizada para recibirlos. Son anulables en el caso del artículo 812.

Artículo 814 Nulidad de testamento común
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Es nulo el testamento otorgado en común por dos o más personas.

